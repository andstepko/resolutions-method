package main

import "gitlab.com/distributed_lab/logan/v3/errors"

func ToDisjunctiveNormForm(sentence Sentence) (Sentence, error) {
	sentence = SubstituteNotBasicOperations(sentence)
	if !sentence.OperatorsAreAllBasic() {
		return Sentence{}, errors.New("some Non-basic Operators has left in the Sentence")
	}

	sentence = DeMorganaLaw(sentence)
	sentence = DistributivityLawForDNF(sentence)

	if !sentence.IsInDNF() {
		return Sentence{}, errors.New("all conversions didn't help to build CNF eventually - that's a bug")
	}

	return sentence, nil
}

func DistributivityLawForDNF(sentence Sentence) Sentence {
	if !sentence.OperatorsAreAllBasic() {
		panic("not all Operators are Basic during DistributivityLaw function")
	}

	if !sentence.HasDisjunction() {
		return sentence
	}

	switch *sentence.Operator {
	case Disjunction:
		left := DistributivityLaw(*sentence.Left)
		right := DistributivityLaw(*sentence.Right)
		sentence.Left = &left
		sentence.Right = &right
		return sentence
	case Conjunction:
		switch {
		case !sentence.Left.IsJustTerm() && *sentence.Left.Operator == Disjunction:
			left := DistributivityLaw(NewSentence(false, *sentence.Left.Left, Conjunction, *sentence.Right))
			right := DistributivityLaw(NewSentence(false, *sentence.Left.Right, Conjunction, *sentence.Right))
			return NewSentence(false, left, Disjunction, right)
		case !sentence.Right.IsJustTerm() && *sentence.Right.Operator == Disjunction:
			left := DistributivityLaw(NewSentence(false, *sentence.Left, Conjunction, *sentence.Right.Left))
			right := DistributivityLaw(NewSentence(false, *sentence.Left, Conjunction, *sentence.Right.Right))
			return NewSentence(false, left, Disjunction, right)
		default:
			left := DistributivityLaw(*sentence.Left)
			right := DistributivityLaw(*sentence.Right)
			sentence.Left = &left
			sentence.Right = &right
			return DistributivityLaw(sentence)
		}
	default:
		panic("not all Operators are Basic during DistributivityLaw function")
	}

	return sentence
}
