package main

import (
	"testing"
	"fmt"
)

func TestDistributivityLaw(t *testing.T) {
	fmt.Println("Distributivity Law conversion function test:")

	s := NewSentence(
		false,
		NewSentence(
			false,
			NewTermSentence("A"),
			Disjunction,
			NewSentence(
				false,
				NewTermSentence("B"),
				Conjunction,
				NewTermSentence("C"),
			),
		),
		Disjunction,
		NewSentence(
			false,
			NewTermSentence("D"),
			Disjunction,
			NewSentence(
				false,
				NewTermSentence("E"),
				Conjunction,
				NewTermSentence("F"),
			),
		),
	)
	fmt.Println(s.String())

	s = DistributivityLaw(s)
	fmt.Println(s.String())
}
