package main

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func Wong(clause Clause) error {
	cnfSentence, err := ToConjunctiveNormForm(clause.Reason)
	if err != nil {
		return errors.Wrap(err, "failed to convert to CNF")
	}
	fmt.Println("CNF:")
	fmt.Println(cnfSentence.String())
	dnfSentence, err := ToDisjunctiveNormForm(clause.Consequence)
	if err != nil {
		return errors.Wrap(err, "failed to convert to CNF")
	}
	fmt.Println("CNF:")
	fmt.Println(dnfSentence.String())

	var conjuncts = cnfSentence.ToConjunctionsList()
	conjuncts = filterDuplications(conjuncts)
	conjuncts = filterTrues(conjuncts)
	printSentences(conjuncts)
	var disjuncts = dnfSentence.ToDisjunctionsList()
	disjuncts = filterDuplications(disjuncts)
	disjuncts = filterTrues(disjuncts)
	printSentences(disjuncts)

	proven := doSearch(conjuncts, disjuncts, false)

	if proven {
		fmt.Println("Proven for initial clause!")
	} else {
		fmt.Println("Not proven for initial clause! ")
	}

	return nil
}

func doSearch(conjuncts []Sentence, disjuncts []Sentence, isReverse bool) bool {
	fmt.Println("Started search for clause: ")
	printClause(conjuncts, disjuncts)

	conLength := len(conjuncts)
	disLength := len(disjuncts)

	for i := 0; i < conLength; i++ {
		current := conjuncts[i]
		if !current.IsJustTerm() {
			conjuncts[i] = *current.Left
			foundLeft := doSearch(conjuncts, disjuncts, isReverse)
			if !foundLeft {
				fmt.Println("Not proven for clause: ")
				printClause(conjuncts, disjuncts)
				return false
			}
			conjuncts[i] = *current.Right
			foundRight := doSearch(conjuncts, disjuncts, isReverse)
			if !foundRight {
				fmt.Println("Not proven for clause: ")
				printClause(conjuncts, disjuncts)
				return false
			} else {
				fmt.Println("Proven for clause: ")
				printClause(conjuncts, disjuncts)
				return true
			}
		} else {
			for j := 0; j < disLength; j++ {
				currentD := disjuncts[j]
				if currentD.IsJustTerm() && *current.Term == *currentD.Term && current.IsNegated == currentD.IsNegated {
					fmt.Println("Proven for clause: ")
					printClause(conjuncts, disjuncts)
					return true
				}
			}

			for j := i + 1; j < conLength; j++ {
				currentC := conjuncts[j]
				if currentC.IsJustTerm() && *current.Term == *currentC.Term && current.IsNegated != currentC.IsNegated {
					fmt.Println("Proven for clause: ")
					printClause(conjuncts, disjuncts)
					return true
				}
			}
		}
	}

	if isAllJustTerms(conjuncts) && !isReverse {
		return doSearch(disjuncts, conjuncts, true)
	}

	return false
}

func printClause(conjuncts []Sentence, disjuncts []Sentence) {
	for _, s := range conjuncts { fmt.Print(s.String())
		fmt.Print(", ")
	}
	fmt.Print(" => ")

	for _, s := range disjuncts {
		fmt.Print(s.String())
		fmt.Print(", ")
	}
	fmt.Println()
}

func isAllJustTerms(sentences []Sentence) bool {
	for _, s := range sentences {
		if !s.IsJustTerm() {
			return false
		}
	}

	return true
}
