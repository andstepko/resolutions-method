package main

import (
	"testing"
	"fmt"
)

var (
	example = Clause{
		Reason: NewSentence(false, NewSentence(
			false,
			NewSentence(
				false,
				NewTermSentence("A", true),
				Implication,
				NewTermSentence("B"),
			),
			Conjunction,
			NewSentence(
				false,
				NewTermSentence("C"),
				Disjunction,
				NewTermSentence("A"),
			),
		),
			Conjunction,
			NewSentence(
				false,
				NewTermSentence("B"),
				Implication,
				NewTermSentence("C", true),
			),
		),
		Consequence: NewTermSentence("A"),
	}

	experiment = NewSentence(
		false,
		NewSentence(false, NewSentence(
			false,
			NewSentence(
				false,
				NewTermSentence("A", true),
				Implication,
				NewTermSentence("B"),
			),
			Conjunction,
			NewSentence(
				false,
				NewTermSentence("C"),
				Disjunction,
				NewTermSentence("A"),
			),
		),
			Conjunction,
			NewSentence(
				false,
				NewTermSentence("B"),
				Implication,
				NewTermSentence("C", true),
			),
		),
		Conjunction,
		NewTermSentence("A", true),
	)
)

// tasks
var (
	t1 = Clause{
		Reason: NewSentence(
			false,
			NewSentence(
				false,
				NewTermSentence("A"),
				Implication,
				NewTermSentence("C"),
			),
			Implication,
			NewSentence(
				false,
				NewTermSentence("A", true),
				Conjunction,
				NewTermSentence("B"),
			),
		),
		Consequence: NewSentence(
			true,
			NewTermSentence("A"),
			Disjunction,
			NewTermSentence("B"),
		),
	}
)

func TestMixDisjuncts(t *testing.T) {
	mixed := mixDisjunctions(NewTermSentence("A"), NewTermSentence("A", true))
	fmt.Println(mixed)
}

func TestResolutions(t *testing.T) {
	//c := t1
	c := example

	//fmt.Println("initial:", s.String())
	fmt.Println(c.String())

	Resolutions(c)
}
