package main

import "gitlab.com/distributed_lab/logan/v3/errors"

func ToConjunctiveNormForm(sentence Sentence) (Sentence, error) {
	sentence = SubstituteNotBasicOperations(sentence)
	if !sentence.OperatorsAreAllBasic() {
		return Sentence{}, errors.New("some Non-basic Operators has left in the Sentence")
	}

	sentence = DeMorganaLaw(sentence)
	sentence = DistributivityLaw(sentence)

	if !sentence.IsInCNF() {
		return Sentence{}, errors.New("all conversions didn't help to build CNF eventually - that's a bug")
	}

	return sentence, nil
}

// TODO Equivalence and other Operators
func SubstituteNotBasicOperations(sentence Sentence) Sentence {
	if sentence.IsJustTerm() {
		return sentence
	}

	left := SubstituteNotBasicOperations(*sentence.Left)
	right := SubstituteNotBasicOperations(*sentence.Right)

	if sentence.Operator.IsBasic() {
		return Sentence{
			Operator:  sentence.Operator,
			IsNegated: sentence.IsNegated,
			Left:      &left,
			Right:     &right,
		}
	}

	// Having a non-basic operator
	switch *sentence.Operator {
	case Implication:
		left.ToggleNegated()
		return Sentence{
			Operator: &Disjunction,
			Left:     &left,
			Right:    &right,
		}
		// TODO
		//case Equivalence:
		//	return NewSentence()
	default:
		panic("unable to substitute a not basic Operator")
	}
}

func DeMorganaLaw(sentence Sentence) Sentence {
	if sentence.IsJustTerm() {
		return sentence
	}

	left := DeMorganaLaw(*sentence.Left)
	right := DeMorganaLaw(*sentence.Right)
	sentence.Left = &left
	sentence.Right = &right

	if !sentence.IsNegated {
		// Already not Negated
		return sentence
	}

	var newOperator Operator
	switch *sentence.Operator {
	case Conjunction:
		newOperator = Disjunction
	case Disjunction:
		newOperator = Conjunction
	default:
		panic("non basic operator is not expected in Sentence during DeMorgana")
	}

	sentence.Left.ToggleNegated()
	sentence.Right.ToggleNegated()
	return NewSentence(false, *sentence.Left, newOperator, *sentence.Right)
}

func DistributivityLaw(sentence Sentence) Sentence {
	if !sentence.OperatorsAreAllBasic() {
		panic("not all Operators are Basic during DistributivityLaw function")
	}

	// We also consider that all Negations in the Sentence are only over, not over complex Sentences
	// and don't check it here

	if !sentence.HasConjunction() {
		// No Conjunctions - the Sentence is already just a Term or Disjunction of Terms
		return sentence
	}

	switch *sentence.Operator {
	case Conjunction:
		left := DistributivityLaw(*sentence.Left)
		right := DistributivityLaw(*sentence.Right)
		sentence.Left = &left
		sentence.Right = &right
		return sentence
	case Disjunction:
		// There definitely are some Conjunction(s) in Left or Right (or both).
		switch {
		case !sentence.Left.IsJustTerm() && *sentence.Left.Operator == Conjunction:
			left := DistributivityLaw(NewSentence(false, *sentence.Left.Left, Disjunction, *sentence.Right))
			right := DistributivityLaw(NewSentence(false, *sentence.Left.Right, Disjunction, *sentence.Right))
			return NewSentence(false, left, Conjunction, right)
		case !sentence.Right.IsJustTerm() && *sentence.Right.Operator == Conjunction:
			left := DistributivityLaw(NewSentence(false, *sentence.Left, Disjunction, *sentence.Right.Left))
			right := DistributivityLaw(NewSentence(false, *sentence.Left, Disjunction, *sentence.Right.Right))
			return NewSentence(false, left, Conjunction, right)
		default:
			left := DistributivityLaw(*sentence.Left)
			right := DistributivityLaw(*sentence.Right)
			sentence.Left = &left
			sentence.Right = &right
			return DistributivityLaw(sentence)
		}
	default:
		panic("not all Operators are Basic during DistributivityLaw function")
	}

	return sentence
}
