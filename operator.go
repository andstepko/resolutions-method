package main

// Only Binary logic operations
// Operator is from 0 to 15
type Operator int

var (
	Conjunction Operator = 8
	Equivalence Operator = 9
	Implication Operator = 13
	Disjunction Operator = 14
)

var operatorsToStrings = map[Operator]string {
	Conjunction: "˄",
	Equivalence: "↔",
	Implication: "→",
	Disjunction: "v",
}

// Symmetric
func (o Operator) IsCommutative() bool {
	a := o & 2 == 2
	b := o & 4 == 4

	return a == b
}

// TODO Look for other Associative Operators
func (o Operator) IsAssociative() bool {
	return o == Conjunction || o == Disjunction
}

func (o Operator) IsBasic() bool {
	return o == Conjunction || o == Disjunction
}

func (o Operator) String() string {
	result, ok := operatorsToStrings[o]
	if !ok {
		panic("unknown string representation of the Operator")
	}

	return result
}
