package main

type Sentence struct {
	Term      *string
	IsNegated bool
	Operator  *Operator
	Left      *Sentence
	Right     *Sentence
}

func NewSentence(isNegated bool, left Sentence, operator Operator, right Sentence) Sentence {
	return Sentence{
		Term:      nil,
		IsNegated: isNegated,
		Operator:  &operator,
		Left:      &left,
		Right:     &right,
	}
}

func NewTermSentence(name string, isNegated ...bool) Sentence {
	var negated bool
	if len(isNegated) > 0 {
		negated = isNegated[0]
	}

	return Sentence{
		Term:      &name,
		IsNegated: negated,
		Operator:  nil,
		Left:      nil,
		Right:     nil,
	}
}

func (s Sentence) IsJustTerm() bool {
	return s.Term != nil && s.Operator == nil && s.Left == nil && s.Right == nil
}

func (s Sentence) IsDisjunct() bool {
	if s.IsJustTerm() {
		// Just a term - is Disjunct with a single member.
		return true
	}

	return *s.Operator == Disjunction && s.Left.IsDisjunct() && s.Right.IsDisjunct()
}

// OperatorsAreAllBasic returns if only Basic(Conjunction and Disjunction) Operators are used in the Sentence.
func (s Sentence) OperatorsAreAllBasic() bool {
	if s.IsJustTerm() {
		// Term does not have Operators, so can't be any Non-basic Operators
		return true
	}

	return s.Operator.IsBasic() && s.Left.OperatorsAreAllBasic() && s.Right.OperatorsAreAllBasic()
}

func (s Sentence) Depth() int {
	if s.IsJustTerm() {
		return 0
	}

	left := s.Left.Depth()
	right := s.Right.Depth()

	if left > right {
		return left + 1
	} else {
		return right + 1
	}
}

func (s Sentence) ToConjunctionsList() []Sentence {
	if s.IsJustTerm() || *s.Operator != Conjunction {
		// Single element slice
		return []Sentence{s}
	}

	// Having a Complex Sentence with Conjunction as Operator
	return append(s.Left.ToConjunctionsList(), s.Right.ToConjunctionsList()...)
}

func (s Sentence) ToDisjunctionsList() []Sentence {
	if s.IsJustTerm() || *s.Operator != Disjunction {
		// Single element slice
		return []Sentence{s}
	}

	// Having a Complex Sentence with Conjunction as Operator
	return append(s.Left.ToDisjunctionsList(), s.Right.ToDisjunctionsList()...)
}

func NewDisjunctionSentence(disjuncts []Sentence) Sentence {
	if len(disjuncts) == 0 {
		return Sentence{}
	}

	if len(disjuncts) == 1 {
		return disjuncts[0]
	}

	return NewSentence(
		false,
		disjuncts[0],
		Disjunction,
		NewDisjunctionSentence(disjuncts[1:]),
	)
}

func (s Sentence) IsInCNF() bool {
	conjunctions := s.ToConjunctionsList()

	for _, c := range conjunctions {
		if c.HasConjunction() {
			// This Conjunct is not a Disjunctions (this Conjunct contains Disjunction of Conjunctions)
			return false
		}
	}

	return true
}

func (s Sentence) IsInDNF() bool {
	disjunctions := s.ToDisjunctionsList()

	for _, d := range disjunctions {
		if d.HasDisjunction() {
			return false
		}
	}

	return true
}

func (s Sentence) HasConjunction() bool {
	if s.IsJustTerm() {
		return false
	}

	if *s.Operator == Conjunction {
		return true
	}

	return s.Left.HasConjunction() || s.Right.HasConjunction()
}

func (s Sentence) HasDisjunction() bool {
	if s.IsJustTerm() {
		return false
	}

	if *s.Operator == Disjunction {
		return true
	}

	return s.Left.HasDisjunction() || s.Right.HasDisjunction()
}

func (s *Sentence) ToggleNegated() bool {
	s.IsNegated = !s.IsNegated
	return s.IsNegated
}

func (s Sentence) String() string {
	if s.IsEmpty() {
		return "empty-sentence"
	}

	if s.IsJustTerm() {
		if s.IsNegated {
			return negateString(*s.Term)
		} else {
			return *s.Term
		}
	}

	var left string
	if !s.Left.IsJustTerm() &&
		// different Operators, or Operator is not Associative
		(*s.Left.Operator != *s.Operator || !s.Left.Operator.IsAssociative()) {
		left = "(" + s.Left.String() + ")"
	} else {
		// Don't need parentheses
		left = s.Left.String()
	}

	//var right = "(" + s.Right.String() + ")"
	var right string
	if !s.Right.IsJustTerm() && (*s.Right.Operator != *s.Operator || *s.Operator == Implication) {
		right = "(" + s.Right.String() + ")"
	} else {
		// Don't need parentheses
		right = s.Right.String()
	}

	result := left + s.Operator.String() + right
	if s.IsNegated {
		return negateString(result)
	} else {
		return result
	}
}

func (s Sentence) Equals(s2 Sentence) bool {
	if s.IsEmpty() && s2.IsEmpty() {
		return true
	}
	if s.IsEmpty() || s2.IsEmpty() {
		return false
	}

	if s.IsJustTerm() != s2.IsJustTerm() {
		return false
	}

	if s.IsJustTerm() && s2.IsJustTerm() {
		return *s.Term == *s2.Term && s.IsNegated == s2.IsNegated
	}

	// Both are Complex Sentences
	if *s.Operator != *s2.Operator {
		return false
	}

	return s.Left.Equals(*s2.Left) && s.Right.Equals(*s2.Right)
}

func (s Sentence) IsOppositeTo(s2 Sentence) bool {
	s2.ToggleNegated()
	return s.Equals(s2)
}

func (s Sentence) IsDefinitelyTrue() bool {
	disjunctions := s.ToDisjunctionsList()

	for i := 0; i < len(disjunctions); i++ {
		for j := i + 1; j < len(disjunctions); j++ {
			if disjunctions[i].IsOppositeTo(disjunctions[j]) {
				return true
			}
		}
	}

	return false
}

func (s Sentence) IsEmpty() bool {
	return s == Sentence{}
}

func negateString(s string) string {
	const overline = "\u0305"

	runes := []rune(s)

	var result string
	for _, r := range runes {
		result += overline + string(r)
	}

	return result
}
