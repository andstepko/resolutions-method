package main

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func Resolutions(clause Clause) error {
	t := clause.Consequence
	t.ToggleNegated()

	sentence := NewSentence(
		false,
		clause.Reason,
		Conjunction,
		t,
	)
	fmt.Println("Initial Clause as Sentence:")
	fmt.Println(sentence.String())

	cnfSentence, err := ToConjunctiveNormForm(sentence)
	if err != nil {
		return errors.Wrap(err, "failed to convert to CNF")
	}
	fmt.Println("CNF:")
	fmt.Println(cnfSentence.String())

	conjuncts := cnfSentence.ToConjunctionsList()
	conjuncts = filterDuplications(conjuncts)
	conjuncts = filterTrues(conjuncts)

	printSentences(conjuncts)

	cur := len(conjuncts)

	for i := 0; i < cur; i++ {
		for j := 0; j < len(conjuncts); j++ {
			if i == j {
				continue
			}

			var mixed *Sentence
			if i < j {
				mixed = mixDisjunctions(conjuncts[i], conjuncts[j])
			} else {
				mixed = mixDisjunctions(conjuncts[j], conjuncts[i])
			}
			if mixed == nil {
				continue
			}

			if !checkExistence(*mixed, conjuncts) {
				fmt.Printf("%d. %s,   (%d,%d)\n", cur, mixed.String(), i, j)
				conjuncts = append(conjuncts, *mixed)
				cur += 1

				if mixed.IsEmpty() {
					fmt.Println()
					fmt.Println("Proven!")
					return nil
				}
			}
		}
	}

	fmt.Println()
	fmt.Println("Cannot be proven.")
	return nil
}

func filterDuplications(sentences []Sentence) []Sentence {
	result := make([]Sentence, 0)

	for _, s := range sentences {
		if !checkExistence(s, result) {
			result = append(result, s)
		}
	}

	return result
}

func filterTrues(sentences []Sentence) []Sentence {
	result := make([]Sentence, 0)

	for _, s := range sentences {
		if s.IsDefinitelyTrue() {
			continue
		}

		result = append(result, s)
	}

	return result
}

func checkExistence(sentence Sentence, sentences []Sentence) bool {
	for _, s := range sentences {
		if sentence.Equals(s) {
			return true
		}
	}

	return false
}

func printSentences(sentences []Sentence) {
	fmt.Println("Sentences:")

	for i, s := range sentences {
		fmt.Printf("%d. %s\n", i, s.String())
	}
}

func mixDisjunctions(d1 Sentence, d2 Sentence) *Sentence {
	if d1.HasConjunction() || d2.HasConjunction() {
		panic("disjunct(s) passed into mixDisjunctions have Conjunction(s)")
	}

	dd1 := d1.ToDisjunctionsList()
	dd2 := d2.ToDisjunctionsList()

	for i1, t1 := range dd1 {
		for i2, t2 := range dd2 {
			if t1.IsOppositeTo(t2) {
				result := append(dd1[:i1], dd1[i1+1:]...)
				result = append(result, dd2[:i2]...)
				result = append(result, dd2[i2+1:]...)

				result = filterDuplications(result)

				t := NewDisjunctionSentence(result)
				return &t
			}
		}
	}

	return nil
}
