package main

type Clause struct {
	Reason Sentence
	Consequence Sentence
}

func (c Clause) String() string {
	return c.Reason.String() + "   " + Implication.String() + "   " + c.Consequence.String()
}
