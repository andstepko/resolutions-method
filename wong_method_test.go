package main

import (
	"testing"
	"fmt"
)

var (
	wongExample = Clause{
		Reason:      wongTestReason,
		Consequence: wongTestConsequence,
	}
)

var (
	wongTestConsequence = NewSentence(false,
		NewSentence(false,
			NewTermSentence("W", false),
			Implication,
			NewTermSentence("X", false)),
		Implication,
		NewSentence(false,
			NewTermSentence("Z", false),
			Implication,
			NewTermSentence("X", false)))
)

var (
	wongTestReason = NewSentence(false,
		NewSentence(
			false,
			NewTermSentence("X", false),
			Disjunction,
			NewTermSentence("Y", false)),
		Conjunction,
		NewSentence(
			false,
			NewSentence(
				false,
				NewSentence(
					false,
					NewTermSentence("X", false),
					Implication,
					NewTermSentence("Y", false)),
				Disjunction,
				NewTermSentence("U", false)),
			Conjunction,
			NewSentence(
				false,
				NewTermSentence("Z", false),
				Implication,
				NewSentence(
					false,
					NewTermSentence("Y", false),
					Implication,
					NewTermSentence("W", false)))))
)

func TestWong(t *testing.T) {
	fmt.Println(wongExample.String())
	Wong(wongExample)
}
